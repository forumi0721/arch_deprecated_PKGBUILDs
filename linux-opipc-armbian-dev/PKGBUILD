# Maintainer: StoneCold <forumi0721[at]gmail[dot]com>

_targetname=opipc
_kernelversion=4.10.11
_armbianversion=5.27
_boardname=orangepipc
_branch=dev
_soc=sun8i
_soc_alias=sun8i
_dtb=sun8i-h3-orangepi-pc.dtb
_dtb_overlay=sun8i-h3
_desc="Orange Pi PC (armbian ${_branch})"
pkgname=("uboot-${_targetname}-armbian-${_branch}" "linux-${_targetname}-armbian-${_branch}" "linux-${_targetname}-armbian-${_branch}-headers")
pkgver=${_kernelversion}
pkgrel=1
arch=("armv7h")
url="http://www.armbian.com/"
license=("GPL")
makedepends=("sunxi-tools" "uboot-tools")
options=("!strip" "libtool" "staticlibs")
source=(
	"armbian-boot-extra.tar.xz"
	"armbian-boot-bin.tar.xz"
    "linux-dtb-${_branch}-${_soc_alias}_${_armbianversion}_armhf.deb"
	"linux-headers-${_branch}-${_soc_alias}_${_armbianversion}_armhf.deb"
	"linux-image-${_branch}-${_soc_alias}_${_armbianversion}_armhf.deb"
	"linux-u-boot-${_branch}-${_boardname}_${_armbianversion}_armhf.deb"
	"linux.preset"
	"90-linux.hook"
)
noextract=(
    "linux-dtb-${_branch}-${_soc_alias}_${_armbianversion}_armhf.deb"
	"linux-u-boot-${_branch}-${_boardname}_${_armbianversion}_armhf.deb"
	"linux-image-${_branch}-${_soc_alias}_${_armbianversion}_armhf.deb"
	"linux-headers-${_branch}-${_soc_alias}_${_armbianversion}_armhf.deb"
)
md5sums=(
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
)

prepare() {
	cd ${srcdir}

	for deb in *.deb
	do
		pushd . &> /dev/null
		local subdir="${deb/_${_armbianversion}_armhf.deb/}"
		rm -rf "${subdir}"
		mkdir "${subdir}"
		cd "${subdir}"
		ar p "../${deb}" data.tar.xz | bsdtar xf -
		popd &> /dev/null
	done
}

package_uboot-opipc-armbian-dev() {
	pkgdesc="U-Boot for ${_desc}"
	backup=("boot/boot.cmd" "boot/boot.scr" "boot/armbianEnv.txt")
	depends=("sunxi-tools" "uboot-tools")
	provides=("uboot-sunxi")
	conflicts=("uboot-sunxi")
	install="uboot.install"
	
	# u-boot
	install -dm755 "${pkgdir}/boot"
	install -Dm644 "${srcdir}/linux-u-boot-${_branch}-${_boardname}/usr/lib/linux-u-boot-${_branch}-${_boardname}_${_armbianversion}_armhf/u-boot-sunxi-with-spl.bin" "${pkgdir}/boot/"

	# scripts
	install -dm755 "${pkgdir}/boot"
	cp -ar "${srcdir}/armbian-boot-bin/." "${pkgdir}/boot/"

	# armbian boot extra
	install -dm755 "${pkgdir}/boot"
	install -Dm644 "${srcdir}/armbian-boot-extra/armbianEnv.txt" "${pkgdir}/boot/"
	#install -Dm644 "${srcdir}/armbian-boot-extra/boot.bmp" "${pkgdir}/boot/"
	install -Dm644 "${srcdir}/armbian-boot-extra/boot.cmd" "${pkgdir}/boot/"
	install -Dm644 "${srcdir}/armbian-boot-extra/boot.scr" "${pkgdir}/boot/"
	install -Dm644 "${srcdir}/armbian-boot-extra/bin/${_boardname}.bin" "${pkgdir}/boot/script.bin"
	if [ "${_branch}" = "next" -o "${_branch}" = "dev" ]; then
		touch "${pkgdir}/boot/.next"
	fi
}

package_linux-opipc-armbian-dev() {
	pkgdesc="The Linux Kernel and modules for ${_desc}"
	depends=("coreutils" "linux-firmware" "kmod" "uboot-tools" 'mkinitcpio>=0.7')
	optdepends=("crda: to set the correct wireless channels of your country")
	backup=("etc/mkinitcpio.d/linux.preset")
	provides=("kernel26" "linux=${_kernelversion}")
	conflicts=("linux")
	install="linux.install"

	# kernel
	install -dm755 "${pkgdir}/boot"
	install -Dm644 "${srcdir}/linux-image-${_branch}-${_soc_alias}/boot/System.map-${_kernelversion}-${_soc_alias}" "${pkgdir}/boot/"
	install -Dm644 "${srcdir}/linux-image-${_branch}-${_soc_alias}/boot/config-${_kernelversion}-${_soc_alias}" "${pkgdir}/boot/"
	install -Dm644 "${srcdir}/linux-image-${_branch}-${_soc_alias}/boot/vmlinuz-${_kernelversion}-${_soc_alias}" "${pkgdir}/boot/"
	install -Dm644 "${srcdir}/linux-image-${_branch}-${_soc_alias}/boot/vmlinuz-${_kernelversion}-${_soc_alias}" "${pkgdir}/boot/zImage"
	#ln -s "vmlinuz-${_kernelversion}-${_soc_alias}" "${pkgdir}/boot/zImage"
	mkimage -A arm -O linux -T kernel -C none -a "0x40008000" -e "0x40008000" -n "Linux kernel" -d "${pkgdir}/boot/zImage" "${pkgdir}/boot/uImage"

	# modules
	install -dm755 "${pkgdir}/usr/lib/modules"
	cp -ar "${srcdir}/linux-image-${_branch}-${_soc_alias}/lib/modules/${_kernelversion}-${_soc_alias}" "${pkgdir}/usr/lib/modules/"

	# modules - make room for external modules
	ln -s "../extramodules-${_kernelversion%.*}-${_soc_alias}" "${pkgdir}/usr/lib/modules/${_kernelversion}-${_soc_alias}/extramodules"

	# modules - add real version for building modules and running depmod from post_install/upgrade
	mkdir -p "${pkgdir}/usr/lib/modules/extramodules-${_kernelversion%.*}-${_soc_alias}"
	echo "${_kernelversion}" > "${pkgdir}/usr/lib/modules/extramodules-${_kernelversion%.*}-${_soc_alias}/version"

	# dtb
	install -dm755 "${pkgdir}/boot/dtb"
	install -Dm644 "${srcdir}/linux-dtb-${_branch}-${_soc_alias}/boot/dtb-${_kernelversion}-${_soc_alias}/${_dtb}" "${pkgdir}/boot/dtb/"

	if ls "${srcdir}/linux-dtb-${_branch}-${_soc_alias}/boot/dtb-${_kernelversion}-${_soc_alias}/overlay"/*${_dtb_overlay}* > /dev/null 2>&1 ; then
		install -dm755 "${pkgdir}/boot/dtb/overlay"
		cp -ar "${srcdir}/linux-dtb-${_branch}-${_soc_alias}/boot/dtb-${_kernelversion}-${_soc_alias}/overlay/"*${_dtb_overlay}* "${pkgdir}/boot/dtb/overlay/"
	fi

	# modify install
	sed -i -e "s/KERNEL_VERSION=.*/KERNEL_VERSION=${_kernelversion}-${_soc_alias}/g" "${startdir}/linux.install"

	# install mkinitcpio preset file for kernel
	install -D -m644 "${srcdir}/linux.preset" "${pkgdir}/etc/mkinitcpio.d/linux.preset"
	sed -i -e "s/^ALL_kver=.*/ALL_kver=${_kernelversion}-${_soc_alias}/g" "${pkgdir}/etc/mkinitcpio.d/linux.preset"

	# install pacman hook for initramfs regeneration
    install -Dm644 "${srcdir}/90-linux.hook" "${pkgdir}/usr/share/libalpm/hooks/90-linux.hook"

	## Now we call depmod...
	#ln -s "${pkgdir}/usr/lib" "${pkgdir}/lib"
	#depmod -b "${pkgdir}" -F "${pkgdir}/boot/System.map-${_kernelversion}-${_soc_alias}" "${_kernelversion}-${_soc_alias}"
	#rm -rf "${pkgdir}/lib"

	## /boot
	#for f in ${pkgdir}/boot/*-${_kernelversion}-${_soc_alias}
	#do
	#	cp "${f}" "${f/-${_kernelversion}-${_soc_alias}/}"
	#done
}

package_linux-opipc-armbian-dev-headers() {
	pkgdesc="Header files and scripts for building modules for linux kernel for ${_desc}"
	provides=("linux-headers=${_kernelversion}" "linux-${_targetname}-headers")
	conflicts=("linux-headers")

	install -dm755 "${pkgdir}/usr/src"
	cp -ar "${srcdir}/linux-headers-${_branch}-${_soc_alias}/usr/src/." "${pkgdir}/usr/src/"

	install -dm755 "${pkgdir}/usr/lib/modules"
	cp -ar "${srcdir}/linux-headers-${_branch}-${_soc_alias}/lib/modules/." "${pkgdir}/usr/lib/modules/"
}

